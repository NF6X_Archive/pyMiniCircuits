#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyMiniCircuits.
#
#  pyMiniCircuits is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyMiniCircuits is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyMiniCircuits.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Enumerate attached Mini-Circuits USB-Interfaced RF Power Sensors.

This script is a utility program which can be used to search for attached
sensors, and/or to take a quick reading from them (at lower accuracy,
because signal frequency is not specified for calibration purposes).

It also serves as a simple example of how to use the usb_pwr_sen module."""

from minicircuits.usb_pwr_sen import *

# Get list of all attached sensors
sensorlist = usb_pwr_sen_enum()

if len(sensorlist) == 0:
    print('No Mini-Circuits USB-Interfaced RF Power sensors found.')

else:
    # Print details of each sensor
    n = 0
    for sensor in sensorlist:
        # Increment device count.
        n = n + 1

        # Print details returned by usb_pwr_sen_enum()
        print('Mini-Circuits USB-Interfaced RF Power Sensor {:d}:'.format(n))
        print('    Model:         {:s}'.format(sensor.model))
        print('    Serial Number: {:s}'.format(sensor.sernum))
        print('    Path:          {:s}'.format(str(sensor.path.decode('ascii'))))

        # Open specific device, using its USB path.
        dev = usb_pwr_sen(path = sensor.path)

        # Print sensor temperature and RF power reading.
        # For better accuracy, pass frequency of signal in Hz to get_power().
        print('    Temperature:   {:g} deg. C'.format(dev.get_temp()))
        print('    Power Reading: {:g} dBm'.format(dev.get_power()))

        # Close the sensor.
        dev.close()
        del dev


