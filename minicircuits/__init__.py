#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyMiniCircuits.
#
#  pyMiniCircuits is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyMiniCircuits is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyMiniCircuits.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Python support for Mini-Circuits portable test equipment.

Presently only supports USB-interfaced RF power sensors, because the
author does not have any other Mini-Circuits test equipment."""

__all__       = ['usb_pwr_sen']
__version__   = '1.0.0'
__copyright__ = 'Copyright (C) 2014 Mark J. Blair, released under GPLv3'
__pkg_url__   = 'http://www.nf6x.net/tags/pyMiniCircuits/'
__dl_url__    = 'https://github.com/NF6X/pyMiniCircuits'



