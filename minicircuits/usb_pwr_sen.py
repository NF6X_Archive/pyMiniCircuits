#!/usr/bin/env python
#
##########################################################################
# Copyright (C) 2014 Mark J. Blair, NF6X
#
# This file is part of pyMiniCircuits.
#
#  pyMiniCircuits is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  pyMiniCircuits is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with pyMiniCircuits.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

"""Python support for Mini-Circuits USB-Interfaced RF Power Sensors.

Code has only been tested with model PWR-SEN-4GHS, but it is likely to work
with other models in the series. Please send bug reports to the author.

Public classes defined:

    usb_pwr_sen_info
    usb_pwr_sen
    
Public funtions defined:

    usb_pwr_sen_enum(sernum)
"""


import hidapi
import math

# Make sure that hidapi is initialized
hidapi.hid_init()

# Internal variables
_MINICIRCUITS_VID     = 0x20CE           # USB Vendor ID
_PWR_SEN_PID          = 0x0011           # USB Product ID

# Public classes

class usb_pwr_sen_info(object):
    """Class describing an attached sensor, as returned by usb_pwr_sen_enum().

    usb_pwr_sen_enum() will return a list of zero or more instances of this
    class. The caller can then inspect the sernum and model member variables
    to identify a specific sensor, and then pass the corresponding path value
    when constructing a new usb_pwr_sen instance to open the device."""

    path   = ''
    sernum = ''
    model  = ''

    def __init__(self, path, sernum, model):
        """Initialize the class instance."""

        self.path   = path
        self.sernum = sernum
        self.model  = model


class usb_pwr_sen(object):
    """Class representing an open Mini-Circuits USB-Interfaced RF Power Sensor.

    Construct a new instance of this class to open an attached sensor.
    If only one sensor is plugged in, then you can simply construct the class
    with no arguments. If more than one sensors are plugged in and you need
    to open a specific one, then call usb_pwr_sen_enum() to get a list of
    attached sensors, pick the desired one, and then pass the path of the
    desired sensor to the class constructor to open that specific sensor.

    If you wish to stop using the sensor without exiting the program, call
    this class's close() function before destroying the instance."""

    # Private variables for internal use by the class
    __dev = None                          # The opened hidapi device for this sensor
    __BUF_LEN              = 64           # Buffer length
    __TIMEOUT              = 5000         # Read timeout, milliseconds

    __CMD_SET_MEAS_MODE    = 0x0f
    __CMD_GET_FIRMWARE     = 99
    __CMD_GET_POWER        = 0x66
    __CMD_GET_TEMP         = 0x67
    __CMD_GET_MODEL        = 0x68
    __CMD_GET_SERNUM       = 0x69

    __MODE_LOW_NOISE       = 0
    __MODE_FAST            = 1
    __MODE_FASTEST         = 2


    def __init__(self, path=None):
        """Class constructor.

        Creates a new instance of the class, opening the specified power
        sendor using the hidapi library.

        With no arguments, opens the first sensor found by hidapi with
        matching VID and PID.

        If path is specified, opens the device with the specified system-
        dependent path, as provided by usb_pwr_sen_enum().

        ARGUMENTS:
            path: optional USB path as returned by usb_pwr_sen_enum().

        RETURNS:
            none"""

        global _MINICIRCUITS_VID
        global _PWR_SEN_PID

        if path is not None:
            # Open device with specified path
            self.__dev = hidapi.hid_open_path(path)
        else:
            # Open first device found
            self.__dev = hidapi.hid_open(vendor_id  = _MINICIRCUITS_VID,
                                         product_id = _PWR_SEN_PID)

        assert self.__dev is not None


    def __get_bytearray(self, command):
        """Internal-use function: Send command and get bytearray response."""

        assert self.__dev is not None

        # Send command
        buf = bytearray(self.__BUF_LEN)
        buf[0] = command
        hidapi.hid_write(self.__dev, buf)

        # Get response
        reply = hidapi.hid_read_timeout(self.__dev, self.__BUF_LEN, self.__TIMEOUT)
        return reply
        

    def __get_string(self, command):
        """Internal-use function: Send command and get string response."""

        # Send command and get response as a bytearray
        reply = self.__get_bytearray(command)
        if len(reply) < 2:
            raise RuntimeError('Error reading string response from sensor.')

        # Extract string from bytearray. Garbage characters may be present
        # after terminating NUL, so we can't just do a simple str() cast.
        thestring = ''
        for c in reply[1:]:
            if c == 0:
                return thestring
            else:
                thestring = thestring + chr(c)
        return thestring
        

    def close(self):
        """Close the power sensor.

        Call this function before destroying the class instance, if the
        sensor may need to be opened again before exiting the program.

        ARGUMENTS:
            none

        RETURNS:
            none"""

        hidapi.hid_close(self.__dev)
        self.__dev = None


    def get_sernum(self):
        """Get the sensor's serial number string.

        ARGUMENTS:
            none

        RETURNS:
            String containing the sensor's serial number.
            Raises RuntimeError exception if error occurs."""

        return self.__get_string(self.__CMD_GET_SERNUM)


    def get_model(self):
        """Get the sensor's model string.

        ARGUMENTS:
            none

        RETURNS:
            String containing the sensor's model.
            Raises RuntimeError exception if error occurs."""

        return self.__get_string(self.__CMD_GET_MODEL)


    def get_firmware(self):
        """Get the sensor's firmware revision.

        This function may not be supported by older firmware revisions.
        The author has a model PWR-SEN-4GHS with a 2012 calibration due
        date which does not appear to respond properly to this function,
        and this command was not included in the programming manual available
        when the sensor was originally purchased.

        This command is listed in the programming manual downloaded when this
        module was written in late 2014, so it is assumed to be added in later
        firmware versions.

        ARGUMENTS:
            none

        RETURNS:
            Two-character string containing the sensor's firmware revision.
            Raises RuntimeError exception if error occurs."""

        buf = self.__get_bytearray(self.__CMD_GET_FIRMWARE)
        if len(buf) < 7:
            raise RuntimeError('Error reading firmware revision.')
        return str(buf[5:7])


    def get_power(self, freq=0):
        """Perform an RF power measurement.

        ARGUMENTS:
            freq: Frequency of signal in Hz, for calibration purposes.
                  Omit this argument is signal frequency is unknown, or specify
                  signal frequency for better accuracy. The sensor cannor measure
                  the signal frequency, but it uses this value to select an offset
                  from its internal calibration table.

        RETURNS:
            Power reading in dBm (floating point value).
            Valid range varies with sensor model.
            For a model PWR-SEN-4GHS, normal range will be -30.0 to +20.0,
            with -99.0 returned when power is below measurable range.
            Raises RuntimeError exception if error occurs."""

        assert self.__dev is not None
        buf = bytearray(self.__BUF_LEN)
        buf[0] = self.__CMD_GET_POWER

        # Convert frequency to integer number of MHz or kHz.
        if freq >= 1.0E6:
            mhz = int(freq / 1.0E6)
            buf[1] = int(math.floor(mhz/256))
            buf[2] = int(mhz - (buf[1] * 256))
            buf[3] = ord('M')
        else:
            khz = int(freq / 1.0E3)
            buf[1] = int(math.floor(khz/256))
            buf[2] = int(khz - (buf[1] * 256))
            buf[3] = ord('K')

        # Send command and get result
        hidapi.hid_write(self.__dev, buf)
        reply = hidapi.hid_read_timeout(self.__dev, self.__BUF_LEN, self.__TIMEOUT)
        if len(reply) < 7:
            raise RuntimeError('Error reading power.')
        return float(reply[1:7])


    def get_temp(self):
        """Get the sensor's internal temperature.

        ARGUMENTS:
            none

        RETURNS:
            Sensor's internal temperature in deg. C (floating point value).
            Raises RuntimeError exception if error occurs."""

        buf = self.__get_bytearray(self.__CMD_GET_TEMP)
        if len(buf) < 7:
            raise RuntimeError('Error reading temperature.')
        return float(buf[1:7])


    def __set_mode(self, mode):
        """Internal-use function: Send mode setting command."""

        assert self.__dev is not None
        buf = bytearray(self.__BUF_LEN)
        buf[0] = self.__CMD_SET_MEAS_MODE
        buf[1] = mode
        hidapi.hid_write(self.__dev, buf)
        hidapi.hid_read_timeout(self.__dev, self.__BUF_LEN, self.__TIMEOUT)


    def set_mode_lownoise(self):
        """Set low-noise mode.

        This function does not apply to model PWR-SEN-6G (now discontinued).

        ARGUMENTS:
            none

        RETURNS:
            none
            Raises RuntimeError exception if error occurs."""

        self.__set_mode(self.__MODE_LOW_NOISE)

    def set_mode_fast(self):
        """Set fast sampling mode.

        This function does not apply to model PWR-SEN-6G (now discontinued).

        ARGUMENTS:
            none

        RETURNS:
            none
            Raises RuntimeError exception if error occurs."""

        self.__set_mode(self.__MODE_FAST)


    def set_mode_fastest(self):
        """Set fastest sampling mode (model PWR-SEN-8GHS only).

        This function only applies to model PWR-SEN-8GHS.

        ARGUMENTS:
            none

        RETURNS:
            none
            Raises RuntimeError exception if error occurs."""

        self.__set_mode(self.__MODE_FASTEST)




# Utility functions

def usb_pwr_sen_enum(sernum=None):
    """Enumerates USB-Interfaced RF Power Sensors attached to system.

    With no arguments, returns a list of usb_pwr_sen_info class instances
    describing the attached sensors.

    If sernum string is specified, returns a list of sensors with matching
    serial numbers (the list should have 0 or 1 elements).

    As a side effect, opens each connected device and queries it for its serial
    number and model. Behavior when this is called while one or more sensors
    is already open is unknown, because the author only has one sensor. To be
    safe, author recommends calling this function before opening any sensors.

    ARGUMENTS:
        sernum: Optional string specifying serial number of desired device.

    RETURNS:
        list of usb_pwr_sen_info class instances.
        List will be empty if no matching devices are found."""

    global _MINICIRCUITS_VID
    global _PWR_SEN_PID

    # Get list of all HID devices with matching VID and PID
    devlist = hidapi.hid_enumerate(vendor_id  = _MINICIRCUITS_VID,
                                   product_id = _PWR_SEN_PID)

    matchlist = []

    # Now open each device, query its model and serial number, and
    # build list to be returned to called.
    for dev in devlist:
        # Open the sensor
        pwrdev = usb_pwr_sen(path=dev.path)

        # Get its serial number and model
        sn = pwrdev.get_sernum()
        mdl = pwrdev.get_model()

        # Record the details
        info = usb_pwr_sen_info(path=dev.path, sernum=sn, model=mdl)

        # Close the sensor
        pwrdev.close()
        del pwrdev

        # Add filtered results to list of matching sensors
        if sernum is None:
            matchlist.append(info)
        else:
            if sernum == info.sernum:
                matchlist.append(info)

    return matchlist

